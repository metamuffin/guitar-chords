module Logic where

import Data.List (findIndex, transpose)

noteName :: Int -> String
noteName 0 = "C"
noteName 1 = "C#"
noteName 2 = "D"
noteName 3 = "D#"
noteName 4 = "E"
noteName 5 = "F"
noteName 6 = "F#"
noteName 7 = "G"
noteName 8 = "G#"
noteName 9 = "A"
noteName 10 = "A#"
noteName 11 = "B"
noteName n = noteName (n `mod` 12)

kindName :: [Int] -> String
kindName k
  | k == major = "maj"
  | k == minor = "min"
  | k == majorseven = "maj7"
  | k == sus4 = "sus4"
  | k == sus2 = "sus2"
  | k == seven = "7"
  | otherwise = "?"

strings = [4, 9, 12 + 2, 12 + 7, 12 + 11, 12 * 2 + 4]

major, minor, seven, sus4, sus2, majorseven :: [Int]
major = [0, 4, 7]
minor = [0, 3, 7]
seven = [0, 4, 7, 10]
sus4 = [0, 5, 7]
sus2 = [0, 2, 7]
majorseven = [0, 4, 7, 11]

pitchUp string note = (note - string) `mod` 12

chord note = map (+ note)

chordPitchUp :: [Int] -> Int -> [Int]
chordPitchUp kind note = notesPitchUp kind $ chord note kind

notesPitchUp :: [Int] -> [Int] -> [Int]
notesPitchUp kind notes = map (minimum . (\s -> map (pitchUp s) notes)) strings
