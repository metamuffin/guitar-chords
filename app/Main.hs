{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Main where

import Diagrams.Backend.SVG.CmdLine
import Diagrams.Prelude
import Diagrams.TwoD.Text
import Diagrams.TwoD.Vector
import Logic

toDouble = fromInteger . toInteger

text' = scaleY (-1) . text

chordPic :: [Int] -> Int -> Diagram B
chordPic k n =
  scale (1 / 10) $
    alignBL (rect 8 10)
      <> translate (V2 4 1) (text' (noteName n ++ kindName k))
      <> translate (V2 1 3) inner
  where
    c = chordPitchUp k n
    inner =
      translate (V2 0.5 0) (mconcat (zipWith stringP [0 .. 5] c))
        <> mconcat (map tabP [0 .. 5])
    stringP n p =
      translate
        (V2 (toDouble n) 0)
        ( (if p /= 0 then point n p else mempty)
            <> fromOffsets [V2 0 6]
            <> translate (V2 0 (-0.5)) (scale 0.7 (text' (noteName (strings !! n))))
        )
    tabP n = translate (V2 0 n) $ fromOffsets [V2 6 0]
    point s p =
      translate
        (V2 0 (toDouble p - 0.5))
        ( text' (noteName ((strings !! s) + p)) # scale 0.3 # fc white
            <> circle 0.25 # fc black
        )

fretOverview :: Diagram B
fretOverview =
  scale (1 / 10) $
    alignBL (rect 8 8)
      <> translate (V2 1 1) inner
  where
    inner =
      translate (V2 0.5 0) (mconcat (map stringP [0 .. 5]))
        <> mconcat (map tabP [0 .. 5])
    stringP n =
      translate
        (V2 (toDouble n) 0)
        ( mconcat (map (point n) [1 .. 5])
            <> fromOffsets [V2 0 6]
            <> translate (V2 0 (-0.5)) (scale 0.7 (text' (noteName (strings !! n))))
        )
    tabP n = translate (V2 0 n) $ fromOffsets [V2 6 0]
    point s p =
      translate
        (V2 0 (toDouble p - 0.5))
        ( text' (noteName ((strings !! s) + p)) # scale 0.3 # fc white
            <> circle 0.25 # fc black
        )

main = mainWith $ scaleY (-1) $ inner # lineWidth 1
  where
    inner = table === licence === scale 3 fretOverview
    licence = alignTL (lineColor white (rect 2 0.2) <> translate (V2 1 0) (scale 0.1 (text' "by metamuffin | CC-BY-SA 4.0")))
    table = foldr1 (flip (===)) $ map (\k -> foldr1 (|||) $ map (chordPic k) [0 .. 11]) kinds
    kinds = [major, minor, seven, majorseven, sus4, sus2]
